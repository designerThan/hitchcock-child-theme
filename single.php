<?php get_header(); ?>

<div class="content section-inner">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class( 'single single-post'); ?>>

		<div class="post-container">

			<?php $post_format = get_post_format(); ?>
			<?php if ( $post_format == 'gallery' ) : ?>

			<div class="featured-media">

				<?php hitchcock_flexslider('post-image'); ?>

				<div class="clear"></div>

			</div>
			<!-- /featured-media -->

			<?php elseif ( $post_format == 'video' ) : ?>
			<?php elseif ( $post_format == 'aside' ) : ?>
			<!--Beitragsbild wird nicht angezeigt-->


			<?php elseif ( has_post_thumbnail() ) : ?>

			<div class="featured-media">

				<?php the_post_thumbnail('post-image'); ?>

			</div>
			<!-- /featured-media -->

			<?php endif; ?>
			
			
			
			
			

			<!--Post Style abhängig von Post Format-->

			<?php if ( $post_format == 'aside' ) : ?>

			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'post-image' ); ?>

			<div class="post-inner-aside" style="background-image: url(<?php echo $thumb['0']; ?>);">

				<div class="post-header-aside">

					<p class="post-date">
						<?php the_time(get_option('date_format')); ?>
					</p>

					<h1 class="post-title">
						<?php the_title(); ?>
					</h1>

				</div>

				<div class="post-content-aside">

					<div class="post-content">
						<?php the_content(); ?>
					</div>

					<div class="post-meta-aside">

						<?php if (has_category()) : ?>
						<p class="categories">
							<?php _e('In','hitchcock'); ?>
							<?php the_category(', '); ?>
						</p>
						<?php endif; ?>
						<?php if (has_tag()) : ?>
						<p class="tags">
							<?php the_tags('', ' '); ?>
						</p>
						<?php endif; ?>

						<?php edit_post_link('Edit Post', '<p class="post-edit-aside">', '</p>'); ?>

					</div>
					<!-- /post-meta -->


					<?php
					$prev_post = get_previous_post();
					$next_post = get_next_post();
					?>

					<div class="post-navigation">

						<?php
						if ( !empty( $prev_post ) ): ?>

						<a class="post-nav-prev" title="<?php echo esc_attr( get_the_title($prev_post) ); ?>" href="<?php echo get_permalink( $prev_post->ID ); ?>">
							<p>
								<?php _e('Next','hitchcock'); ?>
								<span class="hide">
									<?php _e('Post','hitchcock'); ?>
								</span>
							</p>
							<span class="fa fw fa-angle-right"></span>
						</a>

						<?php endif; ?>

						<?php
						if ( !empty( $next_post ) ): ?>

						<a class="post-nav-next" title="<?php echo esc_attr( get_the_title($next_post) ); ?>" href="<?php echo get_permalink( $next_post->ID ); ?>">
								<span class="fa fw fa-angle-left"></span>
								<p><?php _e('Previous','hitchcock'); ?><span class="hide"> <?php _e('Post','hitchcock'); ?></span></p>
							</a>
					

						<?php endif; ?>

						<div class="clear"></div>

					</div>
					<!-- /post-navigation -->
				</div>

				<div class="clear"><span style="width: 100%; visibility: hidden;">Platzhaltertext</span>
				</div>

			</div>



			<?php elseif ( $post_format == 'video' ) : ?>

			<div class="post-inner-video">

				<div class="post-header-video">

					<p class="post-date">
						<?php the_time(get_option('date_format')); ?>
					</p>

					<h1 class="post-title">
						<?php the_title(); ?>
					</h1>

				</div>

				<div class="post-content-video post-content">

					<?php the_content(); ?>

				</div>
				<!-- /post-content -->

				<div class="post-meta-video">

					<?php if (has_category()) : ?>
					<p class="categories">
						<?php _e('In','hitchcock'); ?>
						<?php the_category(', '); ?>
					</p>
					<?php endif; ?>
					<?php if (has_tag()) : ?>
					<p class="tags">
						<?php the_tags('', ' '); ?>
					</p>
					<?php endif; ?>

					<?php edit_post_link('Edit Post', '<p class="post-edit">', '</p>'); ?>

				</div>
				<!-- /post-meta -->

				<?php
				$prev_post = get_previous_post();
				$next_post = get_next_post();
				?>

				<div class="post-navigation">

					<?php
					if ( !empty( $prev_post ) ): ?>

					<a class="post-nav-prev" title="<?php echo esc_attr( get_the_title($prev_post) ); ?>" href="<?php echo get_permalink( $prev_post->ID ); ?>">
						<p>
							<?php _e('Next','hitchcock'); ?>
							<span class="hide">
								<?php _e('Post','hitchcock'); ?>
							</span>
						</p>
						<span class="fa fw fa-angle-right"></span>
					</a>

					<?php endif; ?>

					<?php
					if ( !empty( $next_post ) ): ?>

					<a class="post-nav-next" title="<?php echo esc_attr( get_the_title($next_post) ); ?>" href="<?php echo get_permalink( $next_post->ID ); ?>">
								<span class="fa fw fa-angle-left"></span>
								<p><?php _e('Previous','hitchcock'); ?><span class="hide"> <?php _e('Post','hitchcock'); ?></span></p>
							</a>
				
					<?php endif; ?>

					<div class="clear"></div>

				</div>
				<!-- /post-navigation -->

			</div>

			<?php else : ?>
			<!-- Standard Post Format -->

			<div class="post-header">

				<p class="post-date">
					<?php the_time(get_option('date_format')); ?>
				</p>

				<h1 class="post-title">
					<?php the_title(); ?>
				</h1>

			</div>

			<div class="post-inner">

				<div class="post-content">

					<?php the_content(); ?>

				</div>

				<div class="clear"></div>

				<?php /*?>
				<?php $args = array(
							'before'           => '<div class="page-links"><span class="title">' . __( 'Pages:','hitchcock' ) . '</span>',
							'after'            => '<div class="clear"></div></div>',
							'link_before'      => '<span>',
							'link_after'       => '</span>',
							'separator'        => '',
							'pagelink'         => '%',
							'echo'             => 1
						); 
					
						wp_link_pages($args); 
					?>
				<?php */?>

				<div class="post-meta">

					<?php if (has_category()) : ?>
					<p class="categories">
						<?php _e('In','hitchcock'); ?>
						<?php the_category(', '); ?>
					</p>
					<?php endif; ?>
					<?php /*?>
					<p class="categories">
						<?php _e('Von','hitchcock'); ?>
						<?php wp_list_authors('show_fullname=1&optioncount=1&orderby=post_count&order=DESC&number=3'); ?>
					</p>
					<?php */?>
					<?php if (has_tag()) : ?>
					<p class="tags">
						<?php the_tags('', ' '); ?>
					</p>
					<?php endif; ?>

					<?php edit_post_link('Edit Post', '<p class="post-edit">', '</p>'); ?>

				</div>
				<!-- /post-meta -->

				<?php
				$prev_post = get_previous_post();
				$next_post = get_next_post();
				?>

				<div class="post-navigation">

					<?php
					if ( !empty( $prev_post ) ): ?>

					<a class="post-nav-prev" title="<?php echo esc_attr( get_the_title($prev_post) ); ?>" href="<?php echo get_permalink( $prev_post->ID ); ?>">
						<p>
							<?php _e('Next','hitchcock'); ?>
							<span class="hide">
								<?php _e('Post','hitchcock'); ?>
							</span>
						</p>
						<span class="fa fw fa-angle-right"></span>
					</a>

					<?php endif; ?>

					<?php
					if ( !empty( $next_post ) ): ?>

					<a class="post-nav-next" title="<?php echo esc_attr( get_the_title($next_post) ); ?>" href="<?php echo get_permalink( $next_post->ID ); ?>">
								<span class="fa fw fa-angle-left"></span>
								<p><?php _e('Previous','hitchcock'); ?><span class="hide"> <?php _e('Post','hitchcock'); ?></span></p>
							</a>
				


					<?php endif; ?>

					<div class="clear"></div>

				</div>
				<!-- /post-navigation -->

			</div>

			<?php endif; ?>

			<!-- /post-inner -->

			<?php comments_template( '', true ); ?>

		</div>
		<!-- /post-container -->

	</div>
	<!-- /post -->

</div> <!-- /content -->

<?php hitchcock_related_posts(); ?>

<?php endwhile; else: ?>

<p>
	<?php _e("We couldn't find any posts that matched your query. Please try again.", "hitchcock"); ?>
</p>

<?php endif; ?>

<
/div> <!-- /content -->

<?php get_footer(); ?>