<?php

add_action( 'after_setup_theme', 'hitchcock_setup_child' );

function hitchcock_setup_child() 
{
	// Set content-width
	global $content_width;
	if ( ! isset( $content_width ) ) $content_width = 1300;	
}

function my_theme_enqueue_styles() {

    $parent_style = 'hitchcock_style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function aktiv_postFormats()
{
	add_theme_support( 'post-formats', array( 'gallery', 'video', 'aside' ) );
}
add_action( 'after_setup_theme', 'aktiv_postFormats', 11);

?>