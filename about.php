<?php
/*
Template Name: About Page
*/
?>

<?php get_header(); ?>

<div class="content section-inner about-page">		

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>				
	
		<div <?php post_class('post single'); ?>>
			
			<div class="post-container">
			<div class="post-content ">
			<?php if ( get_bloginfo('description') ) : ?>
			
				<blockquote><?php echo bloginfo('description'); ?></blockquote>
			
			<?php endif; ?>
				</div>
			
			<div class="post-inner">
			    <div class ="about-content">
			    <div class="post-content author-information">
			    	<?php if ( has_post_thumbnail() ) : ?>
			
						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' ); $thumb_url = $thumb['0']; ?>
		
						<div class="featured-media">
							<?php the_post_thumbnail('post-image'); ?>
							<span class="post-content"><?php echo 	get_the_author(); ?></span>
						</div> <!-- /featured-media -->
					<?php endif; ?>
	    		
		    		<?php echo nl2br(get_the_author_meta('description')); ?>
				</div>
		     
			    <div class="post-content blog-information">
			    
			    	<?php the_content(); ?>
					
			    	<?php wp_link_pages('before=<div class="clear"></div><p class="page-links">' . __( 'Pages:', 'hitchcock' ) . ' &after=</p>&seperator= <span class="sep">/</span> '); ?>
			    
			    </div> <!-- /post-content -->
				</div>
			    <div class="clear"></div>
			    
			    <?php edit_post_link(__('Edit Page','hitchcock'), '<div class="post-meta"><p class="post-edit">', '</p></div>'); ?>
	
			</div> <!-- /post-inner -->
			
			<?php comments_template( '', true ); ?>
			
			</div> <!-- /post-container -->
		
		</div> <!-- /post -->
		
	<?php endwhile; else: ?>
	
		<p><?php _e("We couldn't find any posts that matched your query. Please try again.", "hitchcock"); ?></p>

	<?php endif; ?>

	<div class="clear"></div>
	
</div> <!-- /content -->
								
<?php get_footer(); ?>